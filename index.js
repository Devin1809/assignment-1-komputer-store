//This will be the code from the main page
const laptopsElement = document.getElementById("laptops");
const featuresElement = document.getElementById("features");
const laptopImage = document.getElementById("laptopImage");
const laptopTitle = document.getElementById("laptopTitle");
const laptopDescription = document.getElementById("laptopDescription");
const laptopPrice = document.getElementById("laptopPrice");
const buyButton = document.getElementById("buyButton");

const loanBalanceElement = document.getElementById("loanBalance");
const loanButtonElement = document.getElementById("loanButton");
const bankBalanceElement = document.getElementById("bankBalance");
const bankButtonElement = document.getElementById("bankButton");
const workBalanceElement = document.getElementById("workBalance");
const workButtonElement = document.getElementById("workButton");
const payloanButtonElement = document.getElementById("payloanButton");

let laptops = [];
let laptopBuyPrice = 0;

let loanBalance = 0;
let bankBalance = 0;
let workBalance = 0;
let paybackBalance = 0;
const salary = 100;

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => laptops = data)
.then(laptops => addLaptops(laptops));

const addLaptops = (laptops) => {
    laptops.forEach(x => addLaptop(x));
    featuresElement.innerText = laptops[0].specs;
    laptopImage.src = laptops[0].image;
    laptopTitle.innerText = laptops[0].title;
    laptopDescription.innerText = laptops[0].description;
    laptopPrice.innerHTML = `${laptops[0].price} Euros`;
    laptopBuyPrice = laptops[0].price;
}

const addLaptop = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    featuresElement.innerText = selectedLaptop.specs;
    laptopImage.src = selectedLaptop.image;
    laptopTitle.innerText = selectedLaptop.title;
    laptopDescription.innerText = selectedLaptop.description;
    laptopPrice.innerHTML = `${selectedLaptop.price} Euros`;
    laptopBuyPrice = selectedLaptop.price;
}

const buyLaptop = () => {
    if(laptopBuyPrice > bankBalance) {
        alert("Error: You cannot afford this laptop");
    }
    else {
        bankBalance -= laptopBuyPrice;
        bankBalanceElement.innerHTML = `Balance: ${bankBalance} euros`;
        alert("Congratulations! You have just bought a new laptop");
    }
}


const changeLoanBalance = () => {
    const loanToGet = prompt("How much money do you want to loan?");
    if(loanToGet > bankBalance*2) {
        alert("Error: You can only loan twice your bank balance!");
    }
    else if(loanToGet <= 0) {
        alert("Error: Please enter a valid amount to loan");
    }
    else if(isNaN(Math.round(Number(loanToGet))))
    {
        alert("Error: That's not a number!");
    }
    else{ 
        loanBalance = Math.round(loanToGet);
        bankBalance += loanBalance;
        payloanButtonElement.style.visibility = "visible";
        loanButtonElement.style.visibility = "hidden";
        loanBalanceElement.innerHTML = `Loan: ${loanBalance} euros`;
        bankBalanceElement.innerHTML = `Balance: ${bankBalance} euros`;
    }
}

const changeBankBalance = () => {
    if(loanBalance > 0) {
        paybackBalance = workBalance*0.1;
        if(loanBalance > paybackBalance)
        {
            loanBalance -= paybackBalance;
        }
        else {
            paybackBalance = loanBalance;
            loanBalance = 0;
            payloanButtonElement.style.visibility = "hidden";
            loanButtonElement.style.visibility = "visible";
        }
        workBalance -= paybackBalance;  
        loanBalanceElement.innerHTML = `Loan: ${loanBalance} euros`;
    }
    else {
        
    }
    if(loanBalance <= 0)
    {
        loanBalanceElement.innerHTML = ``;
    }
    bankBalance += workBalance; 
    workBalance = 0;
    bankBalanceElement.innerHTML = `Balance: ${bankBalance} euros`;
    workBalanceElement.innerHTML = `Pay: ${workBalance} euros`;
}

const changeWorkBalance = () => {
    workBalance += salary;
    workBalanceElement.innerHTML = `Pay: ${workBalance} euros`;
}

const payLoanBalance = () => {
    if(loanBalance >= workBalance)
    {
       paybackBalance = workBalance;
       loanBalance -= paybackBalance;
       workBalance = 0;
    }
    else {
       paybackBalance = loanBalance;
       loanBalance = 0;
       workBalance -= paybackBalance;
    }
    if(loanBalance > 0) {
        loanBalanceElement.innerHTML = `Loan: ${loanBalance} euros`;
    }
    else {
        loanBalanceElement.innerHTML = ``;
        payloanButtonElement.style.visibility = "hidden";
        loanButtonElement.style.visibility = "visible";
    }
    workBalanceElement.innerHTML = `Pay: ${workBalance} euros`;
}


laptopsElement.addEventListener("change", handleLaptopChange);
buyButton.addEventListener("click", buyLaptop);


loanButtonElement.addEventListener("click", changeLoanBalance);
bankButtonElement.addEventListener("click", changeBankBalance);
workButtonElement.addEventListener("click", changeWorkBalance);
payloanButtonElement.addEventListener("click", payLoanBalance);